package com.omgcode.securityJWT;

import com.omgcode.securityJWT.entity.UserAccount;
import com.omgcode.securityJWT.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class SpringSecurityJwtApplication implements CommandLineRunner {

	public static void main(String[] args) {

		SpringApplication.run(SpringSecurityJwtApplication.class, args);
	}

	@Autowired
	UserRepository userRepository;

	@Override
	public void run(String... args) throws Exception {
		// Insert a user into database when application start.
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		UserAccount user = new UserAccount();
		user.setUsername("admin");
		user.setPassword(passwordEncoder.encode("admin"));
		userRepository.save(user);
		System.out.println(user);
	}
}
