package com.omgcode.securityJWT.configuration;

import com.omgcode.securityJWT.service.UserDetailServiceImp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private UserDetailServiceImp userDetailServiceImp;

    @Override
    protected void doFilterInternal(HttpServletRequest request
            ,HttpServletResponse response
            ,FilterChain filterChain) throws ServletException, IOException {

        try {

            // Get jwt from request
            String jwt = this.getJwtFromRequest(request);

            if(StringUtils.hasText(jwt) && jwtTokenProvider.validateToken(jwt)) {

                // Get user id from JWT
                String userName = jwtTokenProvider.getUserIdFromJWT(jwt);

                // get info of user by ID
                UserDetails userDetail = userDetailServiceImp.loadUserByUsername(userName);

                // Check user is valid or not
                if(userDetail != null) {
                    // If the user is valid, set info for Security Context
                    UsernamePasswordAuthenticationToken authentication
                            = new UsernamePasswordAuthenticationToken(userDetail
                                                                        ,null
                                                                        , userDetail.getAuthorities());
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        }catch (Exception ex) {
            log.error("failed on set user authentication", ex);
        }

        // Filter
        filterChain.doFilter(request, response);
    }

    /**
     * Get JWT from request
     * @param request
     * @return
     */
    private String getJwtFromRequest(HttpServletRequest request) {

        // Get token from request
        String header = request.getHeader("Authorization");

        // Check header Authorization contains JWT or not.
        if (StringUtils.hasText(header) && header.startsWith("Bearer ")) {
            return header.substring(7); // or header.replace("Bearer ", ""); just remove the "Bearer " string.
        }
        return null;
    }
}
