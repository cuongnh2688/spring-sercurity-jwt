package com.omgcode.securityJWT.controller;

import com.omgcode.securityJWT.bean.payload.LoginRequest;
import com.omgcode.securityJWT.bean.payload.LoginResponse;
import com.omgcode.securityJWT.bean.payload.RandomStuff;
import com.omgcode.securityJWT.configuration.CustomUserDetails;
import com.omgcode.securityJWT.configuration.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
public class OMGController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @PostMapping("/login")
    public LoginResponse authenticateUser(@RequestBody LoginRequest loginRequest) {
        // Authentication
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword())
        );

        // set info authentication into Security context
        SecurityContextHolder.getContext().setAuthentication(authentication);

        //Return jwt for customer
        String jwt = jwtTokenProvider.generateToken((CustomUserDetails) authentication.getPrincipal());

        return new LoginResponse(jwt);
    }

    // Api /api/random yêu cầu phải xác thực mới có thể request
    @GetMapping("/random")
    public RandomStuff randomStuff(){
        return new RandomStuff("Authentication success");
    }
}
