package com.omgcode.securityJWT.bean.payload;

import lombok.Data;

@Data
public class LoginResponse {

    private String accessToken;
    private String tokenType;

    public LoginResponse(String accessToken) {
        this.accessToken = accessToken;
        this.tokenType = "Bearer";
    }
}
